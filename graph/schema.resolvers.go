package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"hackernews/auth"
	"hackernews/auth/jwt"
	"hackernews/graph/generated"
	"hackernews/graph/model"
	"hackernews/model/links"
	"hackernews/model/users"
)

func (r *linkResolver) User(ctx context.Context, obj *model.Link) (*model.User, error) {
	user, err := users.GetUserByID(obj.UserID)
	if err != nil {
		return nil, err
	}
	return &model.User{ID: user.ID, Name: user.Username}, nil
}

func (r *mutationResolver) CreateLink(ctx context.Context, input model.NewLink) (*model.Link, error) {
	user := auth.ForContext(ctx)
	if user == nil {
		return &model.Link{}, errors.New("access denied")
	}

	link := links.Link{
		Title: input.Title,
		URL:   input.URL,
		User:  user,
	}

	newlyCreatedLink, err := link.Save()
	if err != nil {
		return nil, err
	}

	createdLink := &model.Link{
		ID:        newlyCreatedLink.ID,
		Title:     newlyCreatedLink.Title,
		URL:       newlyCreatedLink.URL,
		CreatedAt: newlyCreatedLink.CreatedAt,
		UserID:    user.ID,
	}

	r.Mutex.Lock()
	for _, ch := range r.LinkChannels {
		ch <- createdLink
	}
	r.Mutex.Unlock()

	return createdLink, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input model.NewUser) (string, error) {
	user := users.User{
		Username: input.Username,
		Password: input.Password,
	}
	err := user.Create()
	if err != nil {
		return "", err
	}

	token, err := jwt.GenerateToken(user.Username)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (r *mutationResolver) Login(ctx context.Context, input model.Login) (string, error) {
	user := users.User{
		Username: input.Username,
		Password: input.Password,
	}

	correct, err := user.Authenticate()
	if err != nil {
		return "", err
	}

	if !correct {
		return "", errors.New("wrong username or password")
	}

	token, err := jwt.GenerateToken(user.Username)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (r *mutationResolver) RefreshToken(ctx context.Context, input model.RefreshTokenInput) (string, error) {
	username, err := jwt.ParseToken(input.Token)
	if err != nil {
		return "", errors.New("access denied")
	}

	token, err := jwt.GenerateToken(username)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (r *queryResolver) Links(ctx context.Context) ([]*model.Link, error) {
	results := make([]*model.Link, 0)

	dbLinks, err := links.GetAll()
	if err != nil {
		return results, err
	}

	for _, link := range dbLinks {
		results = append(results, &model.Link{
			ID:        link.ID,
			Title:     link.Title,
			URL:       link.URL,
			CreatedAt: link.CreatedAt,
			UserID:    link.User.ID,
		})
	}

	return results, nil
}

func (r *queryResolver) Link(ctx context.Context, id string) (*model.Link, error) {
	link, err := links.GetLinkByID(id)
	if err != nil {
		return nil, err
	}

	return &model.Link{
		ID:        link.ID,
		Title:     link.Title,
		URL:       link.URL,
		CreatedAt: link.CreatedAt,
		UserID:    link.User.ID,
	}, nil
}

func (r *subscriptionResolver) LinkAdded(ctx context.Context, id string) (<-chan *model.Link, error) {
	linkChannel := make(chan *model.Link, 1)
	r.Mutex.Lock()
	r.LinkChannels[id] = linkChannel
	r.Mutex.Unlock()

	go func() {
		<-ctx.Done()
		r.Mutex.Lock()
		delete(r.LinkChannels, id)
		r.Mutex.Unlock()
	}()

	return linkChannel, nil
}

// Link returns generated.LinkResolver implementation.
func (r *Resolver) Link() generated.LinkResolver { return &linkResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type linkResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
