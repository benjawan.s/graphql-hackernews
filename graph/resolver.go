package graph

import (
	"hackernews/graph/model"
	"sync"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	LinkChannels map[string]chan *model.Link
	Mutex        sync.Mutex
}
