package links

import (
	database "hackernews/db/mysql"
	"hackernews/model/users"
	"strconv"
	"time"
)

type Link struct {
	ID        string      `json:"id"`
	Title     string      `json:"title"`
	URL       string      `json:"url"`
	CreatedAt time.Time   `json:"created_at"`
	User      *users.User `json:"user"`
}

func (link Link) Save() (Link, error) {
	stmt, err := database.DB.Prepare("INSERT INTO links(title,url,user_id) VALUES(?,?,?)")
	if err != nil {
		return Link{}, err
	}

	res, err := stmt.Exec(link.Title, link.URL, link.User.ID)
	if err != nil {
		return Link{}, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return Link{}, err
	}

	newlyCreatedLink, err := GetLinkByID(strconv.FormatInt(id, 10))
	if err != nil {
		return Link{}, err
	}

	return newlyCreatedLink, nil
}

func GetAll() ([]Link, error) {
	stmt, err := database.DB.Prepare("select l.id, l.title, l.url, l.created_at, l.user_id, u.username from links l " +
		"inner join users u on l.user_id = u.id " +
		"order by l.created_at desc")
	if err != nil {
		return make([]Link, 0), err
	}

	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return make([]Link, 0), err
	}

	defer rows.Close()

	links := make([]Link, 0)
	for rows.Next() {
		var link Link
		var username string
		var id string

		err := rows.Scan(&link.ID, &link.Title, &link.URL, &link.CreatedAt, &id, &username)
		if err != nil {
			return make([]Link, 0), err
		}

		link.User = &users.User{
			ID:       id,
			Username: username,
		}

		links = append(links, link)
	}

	if err = rows.Err(); err != nil {
		return make([]Link, 0), err
	}

	return links, nil
}

func GetLinkByID(id string) (Link, error) {
	stmt, err := database.DB.Prepare("select sub.id, sub.title, sub.url, sub.created_at, sub.user_id, u.username " +
		"from (select l.* from links l where l.id = ?) sub " +
		"inner join users u on sub.user_id = u.id")
	if err != nil {
		return Link{}, err
	}

	row := stmt.QueryRow(id)

	var link Link
	var username string
	var userID string

	err = row.Scan(&link.ID, &link.Title, &link.URL, &link.CreatedAt, &userID, &username)
	if err != nil {
		return Link{}, err
	}

	link.User = &users.User{
		ID:       userID,
		Username: username,
	}

	return link, nil

}
