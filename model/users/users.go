package users

import (
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	database "hackernews/db/mysql"
)

type User struct {
	ID       string `json:"id"`
	Username string `json:"name"`
	Password string `json:"password"`
}

func (user *User) Create() error {
	statement, err := database.DB.Prepare("INSERT INTO users(username,password) VALUES(?,?)")
	if err != nil {
		return err
	}

	hashedPassword, err := HashPassword(user.Password)
	if err != nil {
		return err
	}

	_, err = statement.Exec(user.Username, hashedPassword)
	if err != nil {
		return err
	}

	return nil
}

func (user *User) Authenticate() (bool, error) {
	statement, err := database.DB.Prepare("select password from users WHERE username = ?")
	if err != nil {
		return false, err
	}

	row := statement.QueryRow(user.Username)

	var hashedPassword string
	err = row.Scan(&hashedPassword)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}

		return false, err
	}

	return CheckPasswordHash(user.Password, hashedPassword), nil
}

func GetUserIdByUsername(username string) (int, error) {
	statement, err := database.DB.Prepare("select id from users WHERE username = ?")
	if err != nil {
		return 0, err
	}

	row := statement.QueryRow(username)

	var id int
	err = row.Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func GetUserByID(id string) (User, error) {
	statement, err := database.DB.Prepare("select id, username from users WHERE id = ?")
	if err != nil {
		return User{}, err
	}

	row := statement.QueryRow(id)

	var user User
	err = row.Scan(&user.ID, &user.Username)
	if err != nil {
		return User{}, err
	}

	return user, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
