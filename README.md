# GraphQL Hackernews

***

This is a tutorial from [how to graphql](https://www.howtographql.com/graphql-go/0-introduction) using GoLang. This
application replicates simple hackernews functionality of posting links with a simple user authentication.

## How to start

***

1. `docker-compose run -d` ***Make sure port 3306 is not already in use.***
2. `go run server.go`
3. Open a browser and go to GraphQL playground at `http://localhost:8080/`

### Examples

***

#### Queries and mutations

```graphql
mutation login {
    login(input: {username: "admin", password: "P@ssw0rd"})
}

mutation refreshToken {
    refreshToken(input: {token: ""})
}

mutation createUser {
    createUser(input: {username: "admin", password:"P@ssw0rd"})
}

mutation createLink {
    createLink(input: {title: "test", url: "https://www.google.com"}) {
        id
        title
        url
        user {
            id
            name
        }
    }
}

query getLinks {
    links {
        id
        title
        url
        user {
            id
            name
        }
    }
}

query getLink {
    link(id: "1") {
        id
        title
        url
        user {
            id
            name
        }
    }
}
```

#### HTTP headers

```json
{
  "Authorization": ""
}
```
