CREATE TABLE IF NOT EXISTS links
(
    id         INT         NOT NULL UNIQUE AUTO_INCREMENT,
    title      VARCHAR(255),
    url        VARCHAR(255),
    created_at DATETIME(6) NOT NULL DEFAULT NOW(6),
    user_id    INT,
    FOREIGN KEY (user_id) REFERENCES users (id),
    PRIMARY KEY (id)
)